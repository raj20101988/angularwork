import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { ELEMENT_DATA } from './tabledata';
import { PeriodicElement } from './tabledata';


@Injectable({
  providedIn: 'root',
})
export class TableDataService {

  constructor() { }

  getTableDataPerPage(range:any): Observable<PeriodicElement[]> {
    // TODO: send the message _after_ fetching the heroes
    let tableDataPerPage = ELEMENT_DATA.slice(range.min,range.max);
    return of(tableDataPerPage);
  }
}