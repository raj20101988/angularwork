import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterwitcheckboxComponent } from './filterwitcheckbox.component';

describe('FilterwitcheckboxComponent', () => {
  let component: FilterwitcheckboxComponent;
  let fixture: ComponentFixture<FilterwitcheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterwitcheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterwitcheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
