import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { FormBuilder, FormControl, FormArray, FormGroup, Validators } from '@angular/forms';
import { CategoryService } from './../category.service';
import { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } from 'constants';
import Item from './item';
import { Garment } from '../categorydata';
@Component({
  selector: 'app-filterwitcheckbox',
  templateUrl: './filterwitcheckbox.component.html',
  styleUrls: ['./filterwitcheckbox.component.scss']
})
export class FilterwitcheckboxComponent implements OnInit,OnChanges {
  @Input() appHighlight:string;
  checked = true;
  indeterminate = false;
  labelPosition = 'after';
  disabled = false;
  items:Item[]=[];
  arrEachCategory:Garment[]=[];
  data={
   options:[
     {id:0,isTrue:false,itemType:"all"}, 
     {id:1,isTrue:false,itemType:"shirt"},
     {id:3,isTrue:false,itemType:"t-shirt"},
     {id:3,isTrue:false,itemType:"jeans"}
    ]
  }
  eachCategoryItem=[];
  checkBoxForm:FormGroup;
  constructor(private fb:FormBuilder, private categoryService:CategoryService) { }

  ngOnInit() {
    this.items=this.data.options
    const controls = this.items.map(c => new FormControl(true));
    this.items=this.items.map((c) => {
      c.id=c.id; c.isTrue=true; c.itemType=c.itemType
      return c;
    });
    

    console.log(this.items);
    this.initCategoryView(this.items);
    this.checkBoxForm = this.fb.group({
    checkBoxOptions: new FormArray(controls, [Validators.required])
  });
  }

  ngOnChanges(){
    this.items=this.data.options
    const controls = this.items.map(c => new FormControl(true));
    this.items=this.items.map((c) => {
      c.id=c.id; c.isTrue=true; c.itemType=c.itemType
      return c;
    });
    

    console.log(this.items);
    this.initCategoryView(this.items);
    this.checkBoxForm = this.fb.group({
    checkBoxOptions: new FormArray(controls, [Validators.required])
  });
    
  }
  get checkBoxOptions1() {
    return this.checkBoxForm.get('checkBoxOptions') as FormArray;
  }
  
  selectOption(objEvt,item){
    // all click logic starts here
    alert(objEvt.checked+":::"+item.itemType);  
    if(item.itemType==="all" && objEvt.checked===true)
    {
      this.checkBoxOptions1.controls.map(x=>x.setValue(objEvt.checked));
      //this.items=this.items.map(c => c.isTrue=objEvt.checked);
      this.items=this.items.map((c) => {
        c.id=c.id; c.isTrue=objEvt.checked; c.itemType=c.itemType
        return c;
      });
     

      
    }else if(item.itemType==="all" && objEvt.checked===false){
      this.checkBoxOptions1.controls.map(x=>x.setValue(objEvt.checked));
      //this.items=this.items.map(c => c.isTrue=objEvt.checked);
      this.items=this.items.map((c) => {
        c.id=c.id; c.isTrue=objEvt.checked; c.itemType=c.itemType
        return c;
      });
    }
    // all click logic ends here

    // indivisual click logic starts here
    else if(item.itemType!=="all" && objEvt.checked===true){
      let allControlValue=this.checkBoxOptions1.controls.map(x=>x.value);
      console.log(allControlValue);
      this.items=this.items.map((c,index) => {
        c.id=c.id; c.isTrue=allControlValue[index]; c.itemType=c.itemType
        return c;
      });

      //console.log("allControlValue="+allControlValue);
      console.log(this.items);
      let allTrue=allControlValue.filter((x=>x==true));
      console.log("allTrue="+allTrue);
      if(allTrue.length>=this.checkBoxOptions1.controls.length-1)
      {
        this.checkBoxOptions1.controls.map(x=>x.setValue(true));
        this.items=this.items.map((c,index) => {
          c.id=c.id; c.isTrue=true; c.itemType=c.itemType
          return c;
        });
      }
    }
    else if(item.itemType!=="all" && objEvt.checked===false){

      let allControlValue=this.checkBoxOptions1.controls.map(x=>x.value);
      console.log("allControlValue="+allControlValue);

      let allFalse=allControlValue.filter((x=>x==false));
      console.log("allFalse="+allFalse);
      if(allFalse.length>=0)
      {
        this.checkBoxOptions1.controls.map((x,index)=>{
          if(index===0)
          x.setValue(false)
          return x
        });

      let allControlValue1=this.checkBoxOptions1.controls.map(x=>x.value);
      console.log("allControlValue1="+allControlValue1);
      this.items=this.items.map((c,index) => {
        c.id=c.id; c.isTrue=allControlValue1[index]; c.itemType=c.itemType
        return c;
      });


      }
    }
    // indivisual click logic ends here
    this.initCategoryView(this.items);
    
}

initCategoryView(viewData){
  
  this.categoryService.getCategoryItem(viewData).subscribe(data=>{
  this.arrEachCategory=[];
    console.log(data);
    if(viewData.length==data.length)
    {
      data.shift();
    }
    
    
    for(var i=0;i<data.length;i++)
    {
      this.arrEachCategory.push(...data[i].eachCategory);
    }

   /* if(objEvt.checked===false)
    {
      this.eachCategoryItem=[];
    }else{
      this.eachCategoryItem=data[0].eachCategory;
    }*/
    

  })
}

}
