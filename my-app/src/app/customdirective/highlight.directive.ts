import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  @Input('appHighlight') highlightColor: string;
  constructor(private el:ElementRef) { }
@HostListener("mouseenter") onMouseEnter() {
  this.highlight(this.highlightColor);
}
@HostListener('mouseout') onmouseout(){
  this.highlight("");
}

highlight(color:string){
  this.el.nativeElement.style.background=color
}

}
