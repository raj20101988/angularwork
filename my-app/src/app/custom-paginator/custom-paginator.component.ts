import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup,FormControl,Validators } from '@angular/forms';


@Component({
  selector: 'app-custom-paginator',
  templateUrl: './custom-paginator.component.html',
  styleUrls: ['./custom-paginator.component.scss']
})
export class CustomPaginatorComponent implements OnInit {

  @Output() emitRange = new EventEmitter();
 
  totalPage=50;
  arrRange = [0,5,10,15,20,25, 49];
  objRange={min:0,max:5}
  //@Output() emitRange=new EventEmitter() as any;
  rowPerPage=new FormControl(this.arrRange[1]);
  range =this.objRange.min+'-'+this.objRange.max;
  rowRangePerPage=new FormControl(this.range);
  constructor() { }

  ngOnInit() {
  }
  selectValue(evt):void{
    //console.log(this.rowPerPage.value);
    this.objRange.max=this.objRange.min+this.rowPerPage.value;
    this.range =this.objRange.min+'-'+this.objRange.max;
    this.rowRangePerPage.setValue(this.range);
  }
  goToFirstPage():void{
    //console.log(this.rowPerPage.value);
    this.objRange.min=this.arrRange[0];
    this.objRange.max=this.arrRange[0]+this.rowPerPage.value;
    this.defineAndEmitRange();
  }
  goToLastPage():void{
    //let lastPage=this.totalPage-1;
    //this.rowPerPage.setValue(lastPage);
    //console.log(this.rowPerPage.value);
    console.log(this.rowPerPage.value);
    this.objRange.min=this.totalPage-this.rowPerPage.value;
    this.objRange.max=this.totalPage;
    this.defineAndEmitRange();
  }
  goToPreviousPage():void{
    //alert('ll');
    if(this.objRange.min>=this.rowPerPage.value)
      {
      this.objRange.min=this.objRange.min-this.rowPerPage.value;
      this.objRange.max=this.objRange.max-this.rowPerPage.value;
      this.defineAndEmitRange();
      }
  }
  goToNextPage():void{
    //alert('pp');
    if(this.objRange.max<=this.totalPage-this.rowPerPage.value)
      {
      this.objRange.min=this.objRange.min+this.rowPerPage.value;
      this.objRange.max=this.objRange.max+this.rowPerPage.value;
      this.defineAndEmitRange();
      }
  }
  defineAndEmitRange():void{
    this.range =this.objRange.min+'-'+this.objRange.max;
    this.rowRangePerPage.setValue(this.range);
    this.emitRange.emit(this.objRange);
  }


}
