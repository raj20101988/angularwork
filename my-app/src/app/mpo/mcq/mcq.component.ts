//for related best example
//https://stackblitz.com/edit/angular-bkvstd?file=src%2Fapp%2Fapp.component.html

import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { MatDatepickerInputEvent, MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.scss']
})
export class McqComponent implements OnInit {
 /*events=[];
  addEvent(type: string, event:MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);
    console.log(event.value);
  }*/

  pager = {
    index:0,
    size:1,
    count:1
  }
  
  mode="";

  isCorrect="wrong";
  ansStatus=false;
  result=false;

  arrFormBuilder=[];
  arrControls=[];
  arrRes:Boolean[]=[];
  showAns=false;
  data={
    questions:[{
      "questionName":"bollywood actress name",
      "optCtrls":[
        {"answer":true ,selected:true},
        {"answer":false, selected:false},
        {"answer":false, selected:false},
        {"answer":false, selected:false}
        ],
        "optLabels":["ajay","alol","anup","shankar"]
    },
    {
      "questionName":"politician name",
      "optCtrls":[
        {"answer":false ,selected:false},
        {"answer":true, selected:false},
        {"answer":false, selected:false},
        {"answer":false, selected:false}
        ],
        "optLabels":["ajay","Amit shah","anup","shankar"]
    },
    {
      "questionName":"bollywood singer name",
      "optCtrls":[
        {"answer":false ,selected:false},
        {"answer":false, selected:false},
        {"answer":false, selected:false},
        {"answer":true, selected:false}
        ],
        "optLabels":["ajay","alol","anup","shankar"]
    }
  ]
}
  quesForm:FormGroup;
  mcqForm:FormGroup;
  allQues:FormGroup[]=[];


constructor(private fb: FormBuilder,iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
  iconRegistry.addSvgIcon(
      'thumbs-up',
      sanitizer.bypassSecurityTrustResourceUrl('assets/img/examples/thumbup-icon.svg'));
}



  ngOnInit() {
    //this.allQues:FormGroup[]=[];


 //for next prev MCQ
  const controls = this.filteredquestions[0].optCtrls.map(c => new FormControl(c.selected));
  this.mcqForm = this.fb.group({
    options: new FormArray(controls, [Validators.required])
  });
  this.pager.count=this.data.questions.length;

  }

  drawAppQuestions(){
    for(let i=0;i<this.data.questions.length;i++)
    {
      this.allQues.push(this.initQuestions(i));
    }
    return this.allQues;
  }

  initQuestions(i):FormGroup{
      return this.fb.group({
        questionName:[this.data.questions[i].questionName],
        quesOptions:this.fb.array(this.initOptions(i)),
      })
  }
  initOptions(i)
  {
    return this.data.questions[i].optCtrls.map(c => new FormControl(c.selected));
    //return this.data.questions[i].optCtrls.map(c => new FormControl(false));
  }

  onSelect(question,qIndex){
    console.log(question.value.quesOptions);// .value provides us jason format of form data, line 124 & 125 is the same
    console.log(question.controls.quesOptions.value);
    question.controls.quesOptions.value.forEach((x,index) => this.data.questions[qIndex].optCtrls[index].selected = x)
    console.log(this.data);
  }

  showsAnswer(){
    this.result=true;
    let res=this.data.questions.map(x=>x.optCtrls.map(y=>y.answer==y.selected));
  console.log(res);
  let arr:Boolean[]=[];
  for(let i=0;i<res.length;i++)
  {
    let isTrue:Boolean;
    let counter=0;
    for(let j=0;j<res[i].length;j++)
    {
      console.log(res[i][j]);
      if(res[i][j]==true)
      {
        counter++;
      }
    }
    if(counter==res[i].length)
      {
        isTrue=true;
      }
      else
      {
        isTrue=false;
      }
      arr.push(isTrue);

  }
  this.arrRes=arr;
  this.showAns=true;

//for all ques at a time
this.quesForm = this.fb.group({
  questions:this.fb.array(this.drawAppQuestions()),  
});


  }

  get questions() {
    return this.quesForm.get('questions') as FormArray;
  }


 

  
//get cities() { return this.resForm.get('cities') as FormArray; } 
updateControls(){
  const controls = this.filteredquestions[0].optCtrls.map(c => new FormControl(c.selected));
  //controls[0].setValue(true);
  this.mcqForm = this.fb.group({
    options: new FormArray(controls, [Validators.required])
  });
}

onSelectOpt(options)
  {
    //console.log(this.form.controls);
    //for (const field in this.mcqForm.controls) { // 'field' is a string
    //const control = this.mcqForm.get(field); // 'control' is a FormControl
   
   //console.log(control);
   this.mcqForm.controls.options.value.forEach((x,index) => options[index].selected = x)
  console.log(this.data.questions);
  //}
  
  }

  isAnswered(options)
  {
    return options.find(x => x.selected) ? true : false;
  }

  submitAns()
  {
      this.isCorrect=this.filteredquestions[0].optCtrls.every(x => x.selected === x.answer) ? 'correct' : 'wrong';
      console.log(':::='+this.isCorrect);
      switch(this.isCorrect)
      {
        case "correct":{
        this.showAns=true;
        //alert(this.showAns);
        break;
        }
        case "wrong":{
        this.showAns=false;
        //alert(this.showAns);
        break;
        }
        default: { 
          //statements; 
         // alert(this.showAns);
          break; 
       } 
      }
     
  }
  

  get filteredquestions(){

    return this.data.questions?
     this.data.questions.slice(this.pager.index,this.pager.index+this.pager.size):[];
  }


  goTo(index)
  {
    
    if(index<this.pager.count)
    {
      this.pager.index=index;
      this.updateControls();
    }
    
    //this.options();
  }

   
   

}
