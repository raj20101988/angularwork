import { Component, OnInit, OnChanges } from '@angular/core';
import { FormGroup,FormArray,FormControl, FormBuilder, ValidatorFn, Validators } from '@angular/forms';
@Component({
  selector: 'app-mcq',
  templateUrl: './mcq.component.html',
  styleUrls: ['./mcq.component.scss']
})
export class McqComponent implements OnInit {
  pager = {
    index:0,
    size:1,
    count:1
  }
  isCorrect="wrong";
  showAns=false;
  ansStatus=false;
  mode="";

  arrFormBuilder=[];
  arrControls=[];

  data={
    questions:[{
      "questionName":"bollywood actress name",
      "options":[
        {"name":"ajay","id":"0","answer":true ,selected:false},
        {"name":"alol","id":"1","answer":false, selected:false},
        {"name":"anup","id":"2","answer":false, selected:false},
        {"name":"shankar","id":"3","answer":false, selected:false}
        ]
    },
    {
      "questionName":"politician name",
      "options":[
        {"name":"ajay","id":"0","answer":false,selected:false},
        {"name":"alol","id":"1","answer":false,selected:false},
        {"name":"anup","id":"2","answer":false,selected:false},
        {"name":"Rahul Gandhi","id":"3","answer":true,selected:false}
        ]
    },
    {
      "questionName":"bollywood singer name",
      "options":[
        {"name":"ajay","id":"0","answer":false,selected:false},
        {"name":"alol","id":"1","answer":false,selected:false},
        {"name":"Abhijit","id":"2","answer":true,selected:false},
        {"name":"anup","id":"3","answer":false,selected:false}
       
        ]
    }
  ]
}

  mcqForm:FormGroup;
  multipleControlInsideFormGroup:FormGroup;
  resForm:FormGroup;
constructor(private fb: FormBuilder) {

console.log(this.filteredquestions[0].options);
  const controls = this.filteredquestions[0].options.map(c => new FormControl(c.selected));
  this.mcqForm = this.fb.group({
    options: new FormArray(controls, [Validators.required])
  });

 let  city=['Shanghai','Zurich'];
 let  citySister=['Shanghai','Zurich'];
 const cityControls = city.map(c => new FormControl(c));
 const citySisControls = citySister.map(c => new FormControl(c));
  this.multipleControlInsideFormGroup = this.fb.group({
    user_id: ['', Validators.required],
    cities: this.fb.array([
      this.fb.group({ 
        name: new FormControl('SF'),
        sisterCities: this.fb.array(cityControls)
      }),
      this.fb.group({
        name: new FormControl('NY'),
        sisterCities: this.fb.array(citySisControls)
      }),
    ]),
  });


  this.resForm = this.fb.group({
      questions:this.initQuestions(),  
  });

}
initQuestions():FormArray{
  return this.fb.array([
    this.fb.group({
      questionName:['my first question'],
      quesOptions:this.initOptions(),
    })
  ])
}
initOptions():FormArray
{
  return this.fb.array(this.data.questions[0].options.map(c => new FormControl(c)));
  //return this.fb.array(this.data.questions[0].options.map(c => new FormControl()));
}

//get cities() { return this.resForm.get('cities') as FormArray; } 
updateControls(){
  const controls = this.filteredquestions[0].options.map(c => new FormControl(c.selected));
  //controls[0].setValue(true);
  this.mcqForm = this.fb.group({
    options: new FormArray(controls, [Validators.required])
  });
}
  ngOnInit() {
    console.log(this.mcqForm.controls)
    this.pager.count=this.data.questions.length;
  }

  checkAnswer(){
  this.mode="result";
  console.log(this.resForm.controls.questions);
  }
  

  onSelect(options)
  {
    //console.log(this.form.controls);
    for (const field in this.mcqForm.controls) { // 'field' is a string
    const control = this.mcqForm.get(field); // 'control' is a FormControl
   
   //console.log(control);
   this.mcqForm.controls.options.value.forEach((x,index) => options[index].selected = x)
  
  }
  console.log(this.data.questions[0].options);
  
  }

  isAnswered(options)
  {
    return options.find(x => x.selected) ? true : false;
  }

  submitAns()
  {
      this.isCorrect=this.filteredquestions[0].options.every(x => x.selected === x.answer) ? 'correct' : 'wrong';
      console.log(':::='+this.isCorrect);
      switch(this.isCorrect)
      {
        case "correct":{
        this.showAns=true;
        //alert(this.showAns);
        break;
        }
        case "wrong":{
        this.showAns=false;
        //alert(this.showAns);
        break;
        }
        default: { 
          //statements; 
         // alert(this.showAns);
          break; 
       } 
      }
     
  }
  

  get filteredquestions(){

    return this.data.questions?
     this.data.questions.slice(this.pager.index,this.pager.index+this.pager.size):[];
  }


  goTo(index)
  {
    
    if(index<this.pager.count)
    {
      this.pager.index=index;
      this.updateControls();
    }
    
    //this.options();
  }

   
   

}
