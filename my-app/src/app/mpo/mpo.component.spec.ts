import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MpoComponent } from './mpo.component';

describe('MpoComponent', () => {
  let component: MpoComponent;
  let fixture: ComponentFixture<MpoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MpoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MpoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
