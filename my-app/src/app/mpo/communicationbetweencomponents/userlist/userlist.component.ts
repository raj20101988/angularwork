import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../users.service';
import { UserDataVo } from '../../../model/UserDataVo';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  public users:UserDataVo[] = [
    /*{ username: 'hhhh', address: '21132 fdfd', email: "fdfd fd", imgPath:"assets/img_chania.jpg" }*/
  ]

  constructor(private usersService:UsersService) { }
  ngOnInit() {
    this.usersService.userSubject.subscribe(objUsers => {
      //this.users=objUsers;
      console.log(objUsers);
      this.users.push(objUsers);
    })
  }

}
