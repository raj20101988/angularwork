import { Component, EventEmitter, Output } from '@angular/core';
import { AllArray } from './all-array';
import { Arr } from './arr';
import { from } from 'rxjs';
@Component({
    selector: 'app-child',
    template: `<button class='btn btn-primary' (click)="valueChanged()">Emit from child to Parent</button> `
})
export class ChildComponent {
    @Output() valueChange = new EventEmitter();
    Counter:number = 0;
    arrAll:AllArray[]=[];
    arr:Arr[]=[{name:"rakmosj",address:"a5555"},{name:"gfgfg",address:"fgf5"},{name:"hjhjh",address:"565hj"}];
    valueChanged() { // You can give any function name
        this.Counter = this.Counter + 1;
        this.arrAll.push({Counter:this.Counter, arrr:this.arr});

        this.valueChange.emit(this.arrAll);
        this.arrAll=[];
    }
}