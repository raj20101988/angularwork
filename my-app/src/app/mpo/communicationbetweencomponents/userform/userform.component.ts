import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../../../users.service';
import { UserDataVo } from '../../../model/UserDataVo';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {
counter:any=0;
submitted=false;
  addUserForm:FormGroup;
   emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  constructor(private fb:FormBuilder,public userService:UsersService) { }
  userData:UserDataVo;
  ngOnInit() {
    //this.userData = this.userService;
    //we can create nested form group.
    
    this.userData=this.userService.usersData;
    this.addUserForm = this.fb.group({
      username: ['', Validators.required],
      address: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  displayCounter(allArr){
      console.log(allArr[0].Counter);
      this.counter=allArr[0].Counter;
      //allArr[0].counter

  }

  get f() { return this.addUserForm.controls; } // returns form input variable to be bound in the template
  
  addUser()
  {
    this.submitted=true;
    if (this.addUserForm.invalid) {
      return;
  }
    console.log(this.f.email.errors)
    //console.log("ppp="+this.f.username.value);
    //console.log("ppp="+this.userData.username);
    this.userData.username = this.f.username.value;
    this.userData.address = this.f.address.value;
    this.userData.email = this.f.email.value;
    this.userService.addUser(this.userData)
    this.submitted=false;
  }
  
}
