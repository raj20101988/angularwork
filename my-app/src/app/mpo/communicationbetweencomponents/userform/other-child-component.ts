import { Component, EventEmitter, Output,Input, OnInit } from '@angular/core';
@Component({
    selector: 'other-app-child',
    template: `<p >my counter updated: {{myCounter}}</p> `
})
export class OtherChildComponent implements OnInit {
    @Input() myCounter:any;
   constructor(){}
   ngOnInit(){
   }
}