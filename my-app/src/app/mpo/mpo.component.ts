import { Component, OnInit } from '@angular/core';
import { UsersService } from '../users.service';
import { UserDataVo } from '../model/UserDataVo';

@Component({
  selector: 'app-mpo',
  templateUrl: './mpo.component.html',
  styleUrls: ['./mpo.component.css']
})
export class MpoComponent implements OnInit {
  public users:UserDataVo[] = [
    { username: 'hhhh', address: '21132 fdfd', email: "fdfd fd", imgPath:"assets/img_chania.jpg" }
  ]
  constructor(private UserService:UsersService) { }

  ngOnInit() {
    this.UserService.userSubject.subscribe(data=>{
      this.users.push(data);
    })
  }

}
