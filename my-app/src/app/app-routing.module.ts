import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddressComponent }  from './address.component';
import { PageNotFoundComponent }  from './page-not-found.component';
import { MpoComponent } from './mpo/mpo.component';

const routes: Routes = [
	{
	   path: 'country',
           loadChildren: 'app/country/country.module#CountryModule'
	},
	{
	   path: 'person',
           loadChildren: 'app/person/person.module#PersonModule'
	},
	{
	   path: 'mpo',
           component: MpoComponent
	},
	{
	   path: 'address',
	   component: AddressComponent
	},	
	{
	   path: '',
	   redirectTo: '/country',
	   pathMatch: 'full'
	},
        {
	   path: '**',
	   component: PageNotFoundComponent 
        }	
];
@NgModule({
  imports: [ 
          RouterModule.forRoot(routes) 
  ],
  exports: [ 
          RouterModule 
  ]
})
export class AppRoutingModule{ } 