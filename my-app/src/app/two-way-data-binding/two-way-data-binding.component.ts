import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ChildIncrDecrComponent } from './child-incr-decr/child-incr-decr.component';

@Component({
  selector: 'app-two-way-data-binding',
  templateUrl: './two-way-data-binding.component.html',
  styleUrls: ['./two-way-data-binding.component.scss']
})
export class TwoWayDataBindingComponent implements OnInit {
  counter:number=0;
  // to use reference variable in ts file and methods and properties of a component, you need to use ViewChild
  @ViewChild('tt') tt: ElementRef;
  @ViewChild(ChildIncrDecrComponent) objChildIncrDecrComponent: ChildIncrDecrComponent;
  constructor() { }
  ngOnInit() { }
  increaseCnt():void{
    this.counter++;
    //console.log("id==="+document.getElementById('pp').getAttribute('id'));
    //document.getElementById("mytext").value = "My value";
    //this.tt.nativeElement.value="ddfdfdfdf";
    //this.objChildIncrDecrComponent.decreaseCounter();
  }
  decrreaseCounter(counter):void{
    this.counter=counter;
  let obj={a:'sfdf',b:'dfdfd', c:'fdfdf'}
  for(let key in obj){
    console.log(key+':::'+obj[key]);
  }

  }

}
