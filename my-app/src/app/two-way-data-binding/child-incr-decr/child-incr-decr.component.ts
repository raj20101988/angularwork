import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-incr-decr',
  templateUrl: './child-incr-decr.component.html',
  styleUrls: ['./child-incr-decr.component.scss']
})
export class ChildIncrDecrComponent implements OnInit {
@Input() counter:number;
@Output() counterChange = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  decreaseCounter():void{
    this.counter--;
    this.counterChange.emit(this.counter);
  }
}
