import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildIncrDecrComponent } from './child-incr-decr.component';

describe('ChildIncrDecrComponent', () => {
  let component: ChildIncrDecrComponent;
  let fixture: ComponentFixture<ChildIncrDecrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildIncrDecrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildIncrDecrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
