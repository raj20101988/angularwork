import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject } from 'rxjs';
import { UserDataVo } from './model/UserDataVo';

@Injectable()
export class UsersService {
  usersData:UserDataVo=new UserDataVo();
   public userSubject=new Subject<any>();
  constructor() { }
  addUser(data){
    data.imgPath="assets/img_chania.jpg";
    this.userSubject.next(data);
  }

}
