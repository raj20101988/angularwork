import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reverseString'
})
export class ReverseStringPipe implements PipeTransform {

  transform(value: string, args?: string): string {
    let str:string='';
    let totalStr=value+args;
    for(let i=totalStr.length;i>=0;i--)
    {
      str+=totalStr.charAt(i);
    }
    return str;
  }

}
