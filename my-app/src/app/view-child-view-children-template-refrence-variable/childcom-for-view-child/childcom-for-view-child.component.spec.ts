import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildcomForViewChildComponent } from './childcom-for-view-child.component';

describe('ChildcomForViewChildComponent', () => {
  let component: ChildcomForViewChildComponent;
  let fixture: ComponentFixture<ChildcomForViewChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildcomForViewChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildcomForViewChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
