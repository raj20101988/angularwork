import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewChildViewChildrenTemplateRefrenceVariableComponent } from './view-child-view-children-template-refrence-variable.component';
import { from } from 'rxjs';
describe('ViewChildViewChildrenTemplateRefrenceVariableComponent', () => {
  let component: ViewChildViewChildrenTemplateRefrenceVariableComponent;
  let fixture: ComponentFixture<ViewChildViewChildrenTemplateRefrenceVariableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewChildViewChildrenTemplateRefrenceVariableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewChildViewChildrenTemplateRefrenceVariableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
