import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, AfterViewChecked } from '@angular/core';
import { ChildcomForViewChildComponent } from './childcom-for-view-child/childcom-for-view-child.component';

@Component({
  selector: 'app-view-child-view-children-template-refrence-variable',
  templateUrl: './view-child-view-children-template-refrence-variable.component.html',
  styleUrls: ['./view-child-view-children-template-refrence-variable.component.scss']
})
export class ViewChildViewChildrenTemplateRefrenceVariableComponent implements OnInit, AfterViewChecked {
  @ViewChild('fName') fName:ElementRef;
  @ViewChild('lName') lName:ElementRef;
  @ViewChild(ChildcomForViewChildComponent) CVCC:ChildcomForViewChildComponent;
  fullName='';
  constructor() { }

  ngOnInit() {
    //console.log('gfgfgf==='+this.fName.nativeElement.value)
  }

  showFullname(f,l){
    this.fullName=f.value+' '+l.value;
  }
  
  ngAfterViewChecked(){
   
    /* let fnn=this.fName.nativeElement.value+' '+this.lName.nativeElement.value;
    console.log('fnn='+fnn);
    if(fnn!=null)
    {
      this.fullName=fnn;
      console.log('this.typedText='+this.fullName);
    } */

    console.log(this.fName.nativeElement.value)//="Rajnish";
    this.lName.nativeElement.value//="Kumar"

    console.log('this.CVCC.childVar='+this.CVCC.childVar);
  }

}
