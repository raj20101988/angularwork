import { NgModule }   from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';
import { AddressComponent }  from './address.component';
import { PageNotFoundComponent }  from './page-not-found.component';
import { MpoComponent } from './mpo/mpo.component';
import { AppRoutingModule }  from './app-routing.module';
import { UserformComponent } from './mpo/communicationbetweencomponents/userform/userform.component';
import { UserlistComponent } from './mpo/communicationbetweencomponents/userlist/userlist.component';
import { UsersService } from './users.service';
import { McqComponent } from './mpo/mcq/mcq.component';
import { MaterialCompoenentModule } from './material-component/Material.component.module';
import { FilterComponent } from './filter/filter.component';
import { ChildComponent } from './mpo/communicationbetweencomponents/userform/child-component';
import { OtherChildComponent } from './mpo/communicationbetweencomponents/userform/other-child-component';
import { PaginatorComponent } from './paginator/paginator.component';
import { TableComponent } from './table/table.component';
import { FilterwitcheckboxComponent } from './filterwitcheckbox/filterwitcheckbox.component';
import { HighlightDirective } from './customdirective/highlight.directive';
import { ReverseStringPipe } from './custompipe/reverse-string.pipe';
import { CustomPaginatorComponent } from './custom-paginator/custom-paginator.component';
import { TwoWayDataBindingComponent } from './two-way-data-binding/two-way-data-binding.component';
import { ChildIncrDecrComponent } from './two-way-data-binding/child-incr-decr/child-incr-decr.component';
import { ViewChildViewChildrenTemplateRefrenceVariableComponent } from './view-child-view-children-template-refrence-variable/view-child-view-children-template-refrence-variable.component';
import { ChildcomForViewChildComponent } from './view-child-view-children-template-refrence-variable/childcom-for-view-child/childcom-for-view-child.component';


@NgModule({
  imports: [     
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MaterialCompoenentModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
		AddressComponent,
		PageNotFoundComponent,
		MpoComponent,
		UserformComponent,
		UserlistComponent,
		McqComponent,
    FilterComponent,
    ChildComponent,
    OtherChildComponent,
    PaginatorComponent,
    TableComponent,
    FilterwitcheckboxComponent,
    HighlightDirective,
    ReverseStringPipe,
    CustomPaginatorComponent,
    TwoWayDataBindingComponent,
    ChildIncrDecrComponent,
    ViewChildViewChildrenTemplateRefrenceVariableComponent,
    ChildcomForViewChildComponent
  ],
  providers: [UsersService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
