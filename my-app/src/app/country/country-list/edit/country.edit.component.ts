import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { switchMap } from 'rxjs/operators';

import { CountryService } from '../../service/country.service';
import { Country } from '../../country';

@Component({
  templateUrl: './country.edit.component.html' 
}) 
export class CountryEditComponent implements OnInit { 
    country: Country;
	constructor(
		private countryService: CountryService,
		private route: ActivatedRoute,
        private router: Router, private fb:FormBuilder) { }
		
    ngOnInit() {
       this.route.params.pipe(
        switchMap((params: Params) => this.countryService.getCountry(+params['country-id'])))
        .subscribe(country => {
		            this.country = country;
					this.setFormValues();
				}
		 );
    }	
	/* countryForm = new FormGroup({
	   name: new FormControl(''),
	   capital: new FormControl(''),
	   currency: new FormControl('')
	});	 */
	countryForm= this.fb.group({
		name: new FormControl(''),
	   capital: new FormControl(''),
	   currency: new FormControl('')
	})

	setFormValues() {
	   this.countryForm.setValue({name: this.country.countryName, 
	      capital: this.country.capital, currency: this.country.currency});
	}	
	onFormSubmit() {
		
		this.country.countryName = this.countryForm.controls.name.value;
	   this.country.capital = this.countryForm.controls.capital.value;
	   this.country.currency = this.countryForm.controls.currency.value;
		
	   this.countryService.updateCountry(this.country)
	     .then(() =>
    		  this.router.navigate([ '../../' ], { relativeTo: this.route })
		 );
	}
}