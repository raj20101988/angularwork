import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { CountryService } from '../service/country.service';
import { Country } from '../country';
@Component({
    templateUrl: './add-country.component.html'
})
export class AddCountryComponent { 
	constructor(
		private countryService: CountryService,
		private route: ActivatedRoute,
        private router: Router, private fb:FormBuilder) { 
			//this.route.outlet.subscribe(data=>console.log(data));
			console.log(this.route.root);

		  }
		
	countryForm = this.fb.group({
		no: new FormControl(),
	   name: new FormControl(''),
	   capital: new FormControl(''),
	   currency: new FormControl('')
	});	
	onFormSubmit() {
		let no = this.countryForm.controls.no.value;
	   let name = this.countryForm.controls.name.value;
	   let capital = this.countryForm.controls.capital.value;
	   let currency = this.countryForm.controls.currency.value;
	   
	   let country = new Country(no, name, capital, currency);
	   this.countryService.addCountry(country)
	      .then(data =>
    		  this.router.navigate([ '../list/view', data.countryId ], { relativeTo: this.route })
		   );
	}
}
    