import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ELEMENT_DATA , PeriodicElement} from './tabledata';
import { from, Subject, BehaviorSubject, ReplaySubject, fromEvent, timer, interval, Observable, of, AsyncSubject} from 'rxjs';
import {switchMap } from 'rxjs/operators';
import { TableDataService } from './table-data.service';
import { map,withLatestFrom } from 'rxjs/operators';



//import { Dummy } from './dummy';
interface Range{
  min:number;
  max:number;
}
 

@Component({
  selector: 'app-root',
  template: `
	<!--<nav [ngClass] = "'parent-menu'">
	  <ul>
		 <li><a routerLink="/country" routerLinkActive="active">Country</a></li>
		 <li><a routerLink="/person" routerLinkActive="active">Person</a></li>
		 <li><a routerLink="/address" routerLinkActive="active">Address</a></li>
		 <li><a routerLink="/mpo" routerLinkActive="active">MPO</a></li>
	  </ul> 
	</nav> -->
	
	<nav [ngClass] = "'parent-menu'" class="navbar navbar-expand-sm bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link"routerLink="/country" routerLinkActive="active">Country</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" routerLink="/person" routerLinkActive="active">Person</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" routerLink="/address" routerLinkActive="active">Address</a>
			</li>
			<li class="nav-item">
        <a class="nav-link" routerLink="/mpo" routerLinkActive="active">MPO</a>
      </li>     
    </ul>
  </div>  
</nav>
  


	<div [ngClass] = "'parent-container'">	
    <router-outlet></router-outlet>	
    <h4> select prices from the select option menu  </h4>
    
    <app-filter></app-filter>

    <data-table [tableDataPerPageProp]="tableDataPerPage" [myCont]="cont"></data-table>
    <app-paginator (emitPageSize)="handlePageSize($event)"></app-paginator>
    <app-custom-paginator (emitRange)="handlePageSizeCustom($event)"></app-custom-paginator>
   
    <p>Items per Page: {{tableDataPerPage}}</p>
    <button (click)="updateCNtr()">counter</button>

   
    <div [appHighlight]="'red'" > I am red highlighted by custom directive </div>
  <div> formated by custom pipe {{str|reverseString:" rajnish kumar"}}</div>


  <app-filterwitcheckbox [appHighlight] ></app-filterwitcheckbox>
  <app-two-way-data-binding></app-two-way-data-binding>
  <app-view-child-view-children-template-refrence-variable></app-view-child-view-children-template-refrence-variable>
  <button id='btn'>from event </button>

  <div id="sub"></div>
  <input type='text' [formControl]='name' />
  <input type='text' [formControl]='id' />
  <button (click)='showValue()'> show value</button>
  <p><label>Name:{{name.value}}</label></p>
  <p><label>Id:{{id.value}}</label></p>

  </div>
  `
})

export class AppComponent implements OnInit { 
str:string="jay jay";
  itemsPerPage:Range={min:0,max:10};
  cont:number=0;
  tableDataPerPage:PeriodicElement[]=ELEMENT_DATA.slice(this.itemsPerPage.min,this.itemsPerPage.max);
   
  
  constructor(private tableDataService:TableDataService){}
  ngOnInit(){
    //console.log(this.tableDataPerPage);
   //console.log('dfdfdfd='+this.objDummy.returnName());

    this.loadInitialTableData();

/* R&D */

    const subject1 = new Subject();
    const subject2=new BehaviorSubject(0);
    const subject3=new ReplaySubject(2);
    const subject4=new AsyncSubject();
    
    var obse1=subject4.subscribe(x => addItem(x));
    //obse1.unsubscribe();
    subject4.next(1);
    subject4.next(2);
    subject4.next(3);
    var obse2=subject4.subscribe(x => addItem(x));
    subject4.next(4);
    subject4.complete();
    function addItem(val:any) {
      var node = document.createElement("li");
      var textnode = document.createTextNode(val);
      node.appendChild(textnode);
      node.innerText=val;
      //document.getElementById("sub").appendChild(node);
  }

  var prom=new Promise((resolve)=>{
    setTimeout(function(){resolve("rajnidh")},4000);
  });
  prom.then(data=>console.log("fgfgfggg="+data));

    //first type asertion or typecast to any and then to number
    // there are two way to type cast (1). as (2). <>
    let numstr:string='2';
    let num:number = <number><any>numstr;   
    let num1:number = numstr as any as number;
    num++;
    num1++;
    console.log('num='+num+'::'+'num1='+num1);
    console.log(undefined===null);
    console.log(20+2+'4'+10+20);
    console.log(3+5*20/5);
    console.log(20%5===0);

    let map1 = new Map([[1 , 2], [2 ,3 ] ,[4, 5]]) as any; 
    map1.forEach((value, key) => console.log(`key: ${key}, value: ${value}`));
    const m = new Map([['color', 'red'], ['owner', 'Flavio'], ['age', '2'], ['owner', 'Flavio']]);
    m.set('name','rajnish');
    m.set('name','rajnish');
    for (const [k, v] of m) {
      console.log(k, v)
    }
    for (const [k, v] of m.entries()) {
      console.log(k, v)
    }
    for (const k of m.keys()) {
      console.log(k)
    }
    for (const v of m.values()) {
      console.log(v)
    }
    const obj = { length: 3, 0: 'foo', 1: 'bar', 2: 'baz' };
    const ob=Object.assign({},obj);
    ob[3]='msss';
    console.log(obj);
    console.log(ob);

    var str="rajnish";
    const array = Array.from(str);
    for (const value of array) { 
        console.log(value);
    }
let obser=from(obj);
obser.subscribe(item=>console.log('hhh='+item));
let frmEvt=fromEvent(document,'click');
let frmEvtExam=frmEvt.pipe(map(event=>`Event time: ${event.timeStamp}`));
frmEvtExam.subscribe(val=>console.log(val));

const source =  timer(0, 5000);
//switch to new inner observable when source emits, emit items that are emitted
const example = source.pipe(switchMap(() => interval(500)));
//output: 0,1,2,3,4,5,6,7,8,9...0,1,2,3,4,5,6,7,8
//const subscribe = example.subscribe(val => console.log(val));

var objs = new Observable((observer)=>{
    setTimeout(()=>{observer.next('rajnish')},3000)
});
var obj2=of("ng lint is used to run Static Code analysis of Angular application");

obj2.subscribe(data=>console.log("my name is "+data));


//cloneing array
let arr=[1,4,6,8,9,4,2];
let arr1=arr.slice();
arr1.pop();
console.log(arr);
console.log(arr1);

/*subject is unicast and observable is multicast*/
// our observable, built to fire initially
const observable = new Observable((observer) => {
  observer.next(Math.random());
});

//our subject
const subject = new Subject();

//first subscriber to subject
subject.subscribe((data) => {
console.log(`Subject subscriber 1 recieves ${data}`)
})

//second subscriber to subject
subject.subscribe((data) => {
console.log(`Subject subscriber 2 recieves ${data}`)
})

//firing the subject
subject.next(Math.random());

//first subscriber to observable
observable.subscribe((data) => {
console.log(`Observable subscriber 1 recieves ${data}`);
});

//second subscriber to observable
observable.subscribe((data) => {
   console.log(`Observable subscriber 2 recieves ${data}`); 
});
/*subject is unicast and observable is multicast*/
    /* R&D */
    this.testwithLatestFrom();
  }


  name=new FormControl('');
  id= new FormControl('');
  showValue(){
    console.log(this.name.value+'::::'+this.id.value);
  }

  loadInitialTableData():void{
    this.tableDataService.getTableDataPerPage(this.itemsPerPage).subscribe(dataPerPage=>this.tableDataPerPage=dataPerPage);
  }
  
  handlePageSize(range):void{
  //this.itemsPerPage=;
  this.itemsPerPage=range;
  //this.tableDataPerPage=ELEMENT_DATA.slice(this.itemsPerPage.min,this.itemsPerPage.max);
  this.tableDataService.getTableDataPerPage(this.itemsPerPage).subscribe(dataPerPage=>this.tableDataPerPage=dataPerPage);
  //console.log(this.tableDataPerPage);
  }

  handlePageSizeCustom(range):void{
   // this.itemsPerPage=null;
  this.itemsPerPage=range;
  //this.tableDataPerPage=ELEMENT_DATA.slice(this.itemsPerPage.min,this.itemsPerPage.max);
  this.tableDataService.getTableDataPerPage(this.itemsPerPage).subscribe(dataPerPage=>this.tableDataPerPage=dataPerPage);
  //console.log(this.tableDataPerPage);
  }

  updateCNtr():void{
    this.cont++;
    this.itemsPerPage.min=10;
    this.itemsPerPage.max=20;
    this.tableDataService.getTableDataPerPage(this.itemsPerPage).subscribe(dataPerPage=>this.tableDataPerPage=dataPerPage);
    //this.tableDataPerPage=ELEMENT_DATA.slice(this.itemsPerPage.min,this.itemsPerPage.max);
    //console.log(this.tableDataPerPage);
  }

testwithLatestFrom()
{
  
  
  //emit every 5s
  const source = interval(5000);
  //emit every 1s
  const secondSource = interval(1000);
  const example = source.pipe(
    withLatestFrom(secondSource),
    map(([first, second]) => {
      return `First Source (5s): ${first} Second Source (1s): ${second}`;
    })
  );
  /*
    "First Source (5s): 0 Second Source (1s): 4"
    "First Source (5s): 1 Second Source (1s): 9"
    "First Source (5s): 2 Second Source (1s): 14"
    ...
  */
  const subscribe = example.subscribe(val => console.log(val));
}

}
    