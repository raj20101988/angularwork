import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PageEvent } from '@angular/material';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit {
  length:any;
  pageIndex:any;
  pageSize:any;
  itemsPerPage={min:"",max:""};

  @Output() emitPageSize=new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    //this.PageEvent.
  }

  getNext(event:PageEvent) {
    
    this.length = event.length
    this.pageIndex=event.pageIndex;
    this.pageSize=event.pageSize;
    let min:any=this.pageSize*this.pageIndex;
    let max:any=(this.pageSize*this.pageIndex)+this.pageSize;
    //alert('ok='+min+'::'+max);
    //this.itemsPerPage.push(min,max);
    this.itemsPerPage.min=min;
    this.itemsPerPage.max=max;
    
    // call your api function here with the offset
    this.emitPageSize.emit(this.itemsPerPage);
    //this.itemsPerPage=[];
  }

}
