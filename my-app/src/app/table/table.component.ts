import {Component, OnInit, ViewChild, Input, AfterViewChecked, OnDestroy, AfterContentInit, OnChanges} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}



/**
 * @title Table with sorting
 */
@Component({
  selector: 'data-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, OnChanges {
  @Input() tableDataPerPageProp : any
  @Input() myCont:number;
  dataSource:any=null;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  
  constructor(){

    //alert("itemsPerPageProp="+this.tableDataPerPageProp);
  };

  ngOnChanges(){
    console.log(this.tableDataPerPageProp);
    

    this.dataSource = new MatTableDataSource(this.tableDataPerPageProp);
    this.dataSource.sort = this.sort;
  }

  
 
  

  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
   // alert("tableDataPerPageProp="+this.tableDataPerPageProp);
    
    //this.dataSource = new MatTableDataSource(this.tableDataPerPageProp);
    //this.dataSource.sort = this.sort;
  }
  
}


/**  Copyright 2018 Google Inc. All Rights Reserved.
    Use of this source code is governed by an MIT-style license that
    can be found in the LICENSE file at http://angular.io/license */